﻿using System;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("http://localhost:5000");

            var client = new Customers.CustomersClient(channel);

            // test GetCustomers
            Console.WriteLine("Тестируем получение списка клиентов");
            var customersResponse = client.GetCustomers(new Empty());
            Console.WriteLine(customersResponse);
            Console.WriteLine();

            // test GetCustomer
            Console.WriteLine("Тестируем получение клиента по ID");
            var customerResponse = client.GetCustomer(new CustomerIdRequest()
            {
                Id = customersResponse.Customers[0].Id
            });
            Console.WriteLine(customerResponse);
            Console.WriteLine();

            // test CreateCustomer
            Console.WriteLine("Тестируем создание клиента");
            var createCustomerResponse = client.CreateCustomer(new CreateCustomerRequest()
            {
                FirstName = "Alexey",
                LastName = "Moskovkin",
                Email = "mail@mail.com",
                PreferenceIds =
                {
                    "ef7f299f-92d7-459f-896e-078ed53ef99c", 
                    "76324c47-68d2-472d-abb8-33cfa8cc0c84"
                }
            });
            Console.WriteLine(createCustomerResponse);
            Console.WriteLine();

            // test EditCustomer
            Console.WriteLine("Тестируем редактирование клиента");
            var editCustomerResponse = client.EditCustomer(new EditCustomerRequest()
            {
                Id = customersResponse.Customers[0].Id,
                FirstName = "Ivan",
                LastName = "Ivanov",
                Email = "ivan@mail.com",
                PreferenceIds = {  }
            });
            Console.WriteLine(editCustomerResponse);
            customerResponse = client.GetCustomer(new CustomerIdRequest()
            {
                Id = customersResponse.Customers[0].Id
            });
            Console.WriteLine(customerResponse);
            Console.WriteLine();

            // test DeleteCustomer
            Console.WriteLine("Тестируем удаление клиента");
            var deleteCustomerResponse = client.DeleteCustomer(new CustomerIdRequest()
            {
                Id = customersResponse.Customers[0].Id
            });
            Console.WriteLine(deleteCustomerResponse);
        }
    }
}