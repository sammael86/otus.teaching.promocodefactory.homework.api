using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Domain = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Domain.Customer> _customerRepository;
        private readonly IRepository<Domain.Preference> _preferenceRepository;

        public CustomersService(IRepository<Domain.Customer> customerRepository,
            IRepository<Domain.Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomersResponse> GetCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new CustomersResponse();

            response.Customers.AddRange(
                customers.Select(x => new CustomerShortResponse()
                {
                    Id = x.Id.ToString(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                })
            );

            return response;
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerIdRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                return new CustomerResponse();

            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            response.Preferences.AddRange(
                customer.Preferences.Select(x => new Preference()
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                })
            );

            return response;
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateCustomerRequest request,
            ServerCallContext context)
        {
            var preferenceIds = request.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIds);

            var id = Guid.NewGuid();
            var customer = new Domain.Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new Domain.CustomerPreference()
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            var response = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            response.Preferences.AddRange(
                customer.Preferences.Select(x => new Preference()
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                })
            );

            return response;
        }

        public override async Task<Empty> EditCustomer(EditCustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                return new Empty();
            
            var preferenceIds = request.PreferenceIds.Select(Guid.Parse).ToList();
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(preferenceIds);

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null)
                return new Empty();
            
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new Domain.CustomerPreference()
            {
                CustomerId = id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();
            
            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(CustomerIdRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var id))
                return new Empty();
            
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer is null)
                return new Empty();

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}